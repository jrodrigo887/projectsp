package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Helper.Preferencias;
import com.supermarket.projeto.classes.Model.Endereco;
import com.supermarket.projeto.classes.Model.Usuario;

public class CadastroUsuarioActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private BootstrapEditText nome, email, cnpj,celuar, telefone, senha1, senha2;
    private RadioButton rbAdmin, rbForn, rbMei;
    private BootstrapButton btnCadastrar, btnCancelar;
    private FirebaseAuth autenticacao;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Usuario usuario;
    private View view1, view2;
    private LinearLayout linearLayout2, linearLayout1;
    private TextInputLayout inputNome, inputEmail,inputCnpj, inputTel, inputCel, inputSenha1, inputSenha2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Novo Usuário");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //ID´s DOS CAMPOS EDITTEXT
        email   = (BootstrapEditText) findViewById(R.id.idEmailCadastro);
        nome    = (BootstrapEditText) findViewById(R.id.idNomeCadastro);
        senha1  = (BootstrapEditText) findViewById(R.id.idSenha1Cadastro);
        senha2  = (BootstrapEditText) findViewById(R.id.idSenha2Cadastro);
        rbAdmin = findViewById(R.id.rbAdminCadastrar);
        rbForn  = findViewById(R.id.rbFornCadastrar);
        rbMei   = findViewById(R.id.rbMeiCadastrar);
        cnpj = findViewById(R.id.idCnpjCadastro);
        celuar = findViewById(R.id.idCelularCadastro);
        telefone = findViewById(R.id.idTelefoneCadastro);
        btnCadastrar = (BootstrapButton) findViewById(R.id.btnCadastrar);
        btnCancelar = (BootstrapButton) findViewById(R.id.btnCancelCad);

        //ID´S DOS TEXTINPUTLAYOUT
        inputNome = findViewById(R.id.cadNewUserNome);
        inputEmail = findViewById(R.id.cadNewUserEmail);
        inputCnpj = findViewById(R.id.cadNewUserCnpj);
        inputTel = findViewById(R.id.cadNewUserTel);
        inputCel = findViewById(R.id.cadNewUserCel);
        inputSenha1 = findViewById(R.id.cadNewUserSenha1);
        inputSenha2 = findViewById(R.id.cadNewUserSenha2);

        //Validar senha1.
//        senha1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                int senha = Integer.parseInt(senha1.getText().toString());
//
//                if (!hasFocus){
//                    if (inputSenha1.isCounterEnabled()){
//
//                        Toast.makeText(getApplicationContext(), "Senha no máximo 8 carcteres", Toast.LENGTH_LONG).show();
//
//                    }
//                }
//            }
//        });


        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarCamposUsuario();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            //retorna para a tela anterior.
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void validarCamposUsuario(){



        if (senha1.getText().toString().equals(senha2.getText().toString()) && senha1.getText().toString() != ""){
            usuario = new Usuario();
            if (email.getText().toString().equals("")){
                messenge("Por favor preencher o campo de E-mail");
            }else{

                usuario.setEmail(email.getText().toString());
            }
            if (nome.getText().toString().equals("")){
                messenge("Preencher o campo Nome");
            }else {
                usuario.setNome(nome.getText().toString());
            }

            //Vertificar a senha novamente.
            int senha = Integer.parseInt(senha1.getText().toString());
            if (senha <=0 && senha > 8){
                Toast.makeText(getApplicationContext(), "Senha no máximo 8 carcteres", Toast.LENGTH_LONG).show();
            }else{
                usuario.setSenha(senha1.getText().toString());
            }
            usuario.setCelular(celuar.getText().toString());
            usuario.setTelefone(telefone.getText().toString());
            if (cnpj.getText().toString().equals("")){
                messenge("Preencher o campo CNPJ");
            }else {

                usuario.setCnpj(cnpj.getText().toString());

            }
            //Setando endereço vazio para posteriormente o usuário ter a opção de editar o seu perfil e preencher os dados.
            final Endereco end = new Endereco();
            end.setCep("");
            end.setBairro("");
            end.setCidade("");
            end.setNumero("");
            end.setComplemeto("");
            end.setRua("");
            end.setUf("");
            usuario.setEndereco(end);

            //verificando o tipo de usuário.
            if (rbAdmin.isChecked()){
                usuario.setTipoUsuario("Administrador");
            }else if (rbForn.isChecked()){
                usuario.setTipoUsuario("Fornecedor");
            }else if (rbMei.isChecked()){
                usuario.setTipoUsuario("Empresa");

                Log.d("cadastrado", "Tipo de user: !" + usuario.getTipoUsuario());
            }else{
                Toast.makeText(CadastroUsuarioActivity.this, "Selecionar tipo de Usuário!", Toast.LENGTH_LONG).show();
            }

            //Senha correta? então, cadastrar usuário!
            cadastrarUsuario();

        }else{
            Toast.makeText(CadastroUsuarioActivity.this, "Senha não se Correspondem", Toast.LENGTH_LONG).show();
        }

    }
    private void cadastrarUsuario(){

        autenticacao = ConfigFirebase.getAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(CadastroUsuarioActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    inserirUsuario(usuario);
                    finish();

                    //deslogar usuário atual cadastrado.
                    autenticacao.signOut();

                    //Reutenticação do usuário logado
                    abreTelaPrincipal();

                }else{

                    String errException = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        errException = "Digite uma senha no máximo de 8 Caracter com letras e numeros";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        errException = "O e-mail digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e){
                        errException = "E-mail já está cadastrado!";
                    } catch (Exception e) {
                        errException = "Erro ao efetuar o cadastro";
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastroUsuarioActivity.this, "Erro! " + errException, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private boolean inserirUsuario(Usuario user){

        try{

            databaseReference = ConfigFirebase.getReferenceFirebase().child("usuarios");
//            databaseReference.push().setValue(user);

            Log.d("cadastrado", "Cadastro enviado para database!");

            String key = databaseReference.push().getKey();
            user.setKeyUsuario(key);
            databaseReference.child(key).setValue(user);
            Toast.makeText(CadastroUsuarioActivity.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();
            abreTelaPrincipal();

            return true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(CadastroUsuarioActivity.this, "Erro ao gravar usuário" , Toast.LENGTH_LONG).show();

            return false;
        }
    }

    private void abreTelaPrincipal(){
        autenticacao = ConfigFirebase.getAutenticacao();

        Preferencias preferencias = new Preferencias(CadastroUsuarioActivity.this);

        autenticacao.signInWithEmailAndPassword(preferencias.getEmailUsuarioLogado(), preferencias.getSenhaUsuarioLogado()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    Intent intent = new Intent(CadastroUsuarioActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(CadastroUsuarioActivity.this, "Falha", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CadastroUsuarioActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);
                }
            }
        });

    }

    public void messenge(String s){

        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }
}
