package com.supermarket.projeto.classes.FragmentsProduto;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Activity.DescricaoProdutosActivity;
import com.supermarket.projeto.classes.Activity.ListProdutosActivity;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Helper.Preferencias;
import com.supermarket.projeto.classes.Model.Pedidos;
import com.supermarket.projeto.classes.Model.Produto;
import com.supermarket.projeto.classes.Model.Usuario;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.TimeZone;

public class VisaoGeralProdutosFragment extends Fragment {
    private TextView txtMarca, txtDescricao;
    private String strTitulo;
    private ListenerFragVisao fragVisaoListener;
    private Produto p;
    private BootstrapButton btnGerarPedido;
    private DatabaseReference databaseReference;
    private FirebaseAuth autenticacao;
    private Pedidos pedidos = new Pedidos();
    LocalDate dateNow;
    LocalDateTime time;
    private Usuario usuario;

    public VisaoGeralProdutosFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_visao_geral_produtos, container,false);

        txtDescricao = (TextView) view.findViewById(R.id.caixaVazioDescricao);
        txtMarca = (TextView) view.findViewById(R.id.caixaVazioMarca);
        btnGerarPedido = view.findViewById(R.id.btnGerarPedido);
        Bundle bundle = getArguments();

        //Buscar os dados do Produto na Activity.
        DescricaoProdutosActivity descActivity = (DescricaoProdutosActivity) getActivity();
        final Produto pdt = (Produto) Parcels.unwrap(bundle.getParcelable("pdt"));
        //pegando dados produto.
        p = descActivity.getDataProduto();

        if (p == null){
            Log.d("pedido", "Objeto Nulo");
        }else{

            if(p.getDescricao() != null){

                txtDescricao.setText(p.getDescricao());
            }else if(p.getMarca() != null){
                txtMarca.setText(p.getMarca());
            }else{
                txtDescricao.setText("Sem descrição!");
                txtMarca.setText("Marca não descrita.");
            }

        }

        btnGerarPedido.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                cadastrarPedido();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public interface ListenerFragVisao {
        void setOnFragVisao();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try
        {
            fragVisaoListener = (ListenerFragVisao) context;
        }catch(ClassCastException e)
        {
            throw new ClassCastException(context.toString() + "Erro!");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragVisaoListener = null;
    }

    private void cadastrarPedido(){

        usuario = new Usuario();

        Preferencias pref = new Preferencias(getContext());
        usuario.setEmail(pref.getEmailUsuarioLogado());
        usuario.setSenha(pref.getSenhaUsuarioLogado());

        autenticacao = ConfigFirebase.getAutenticacao();
        autenticacao.signInWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(getActivity(),new OnCompleteListener<AuthResult>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    inserirPedido();

                }else{

                    String errException = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthRecentLoginRequiredException e){
                        errException = "Tentantiva de se logar novamente";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        errException = "O e-mail digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e){
                        errException = "E-mail já está cadastrado!";
                    } catch (Exception e) {
                        errException = "Erro ao efetuar o cadastro";
                        e.printStackTrace();
                    }


                }

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void inserirPedido(){

        //Buscando a data atual;
        dateNow = LocalDate.now();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss" );
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        time =  null;
        String tempo = format.format(calendar.getTime());
                //DateFormat.getDateTimeInstance().format(new Date());



                //LocalTime.now();

        //Formatando a data para string
        String strData = String.valueOf(dateNow);
        String strTime = String.valueOf(tempo);

        pedidos.setNomeProduto(p.getTitulo());
        pedidos.setProdutokey(p.getKeyProduto());
        pedidos.setValor(p.getValor());
        pedidos.setFornecedor(p.getFornecedor());
        pedidos.setQuantidade("");
        pedidos.setDateAndTime(strData + " - " + strTime);

        final String usuarioLogado = autenticacao.getCurrentUser().getEmail();
        databaseReference = ConfigFirebase.getReferenceFirebase();

        databaseReference.child("usuarios").orderByChild("email").equalTo(usuarioLogado).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    //Buscar dados do usuário para inserir no pedido
                    final Usuario user = dataSnapshot1.getValue(Usuario.class);

                    pedidos.setEmpresaKey(user.getKeyUsuario());
                    pedidos.setNomeEmpresa(user.getNome());
                    pedidos.setEmailEmpresa(usuarioLogado);
                    persistirPedido();

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private boolean persistirPedido(){

        try{

            databaseReference = ConfigFirebase.getReferenceFirebase().child("pedidos");
//            databaseReference.push().setValue(user);

            Log.d("pedido", " enviado para database!");

            String key = databaseReference.push().getKey();
            pedidos.setPedidoKey(key);
            databaseReference.child(key).setValue(pedidos);
            Toast.makeText(getActivity(), "Pedido registrado com Sucesso!", Toast.LENGTH_LONG).show();
            abreTelaPrincipal();

            return true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getActivity(), "Erro ao registrar pedido" , Toast.LENGTH_LONG).show();

            return false;
        }
    }

    private void abreTelaPrincipal() {

        Intent intent = new Intent(getContext(), ListProdutosActivity.class);
        startActivity(intent);

    }

}
