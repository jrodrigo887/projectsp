package com.supermarket.projeto.classes.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Model.Pedidos;

import java.util.ArrayList;
import java.util.List;

public class PedidoAdapterForn extends RecyclerView.Adapter<PedidoAdapterForn.ViewHolder> {
    private Context context;
    private ArrayList<Pedidos> mylistPedidos;
    private Pedidos allRequest;
    private List<Pedidos> listPedidos;
    DatabaseReference databaseReference;

    public PedidoAdapterForn(Context context, ArrayList<Pedidos> mylistPedidos) {
        this.context = context;
        this.mylistPedidos = mylistPedidos;
    }

    @NonNull
    @Override
    public PedidoAdapterForn.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_pedido_forn, viewGroup, false);

        return new PedidoAdapterForn.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PedidoAdapterForn.ViewHolder holder, int position) {

        final Pedidos item = mylistPedidos.get(position);

        listPedidos = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("produtos").orderByChild("keyProduto").equalTo(item.getPedidoKey()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                listPedidos.clear();


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    allRequest = postSnapshot.getValue(Pedidos.class);

                    listPedidos.add(allRequest);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        holder.pedidoTitulo.setText(item.getNomeProduto());
        holder.pedidoQuantidade.setText(item.getQuantidade());
        holder.pedidoData.setText(item.getDateAndTime());

    }

    @Override
    public int getItemCount() {
        return mylistPedidos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView pedidoTitulo;
        private TextView pedidoPreco;
        private TextView pedidoQuantidade;
        private TextView pedidoData;
        private TextView pedidoFornecedor;
        private TextView pedidoEmpresa;

        public ViewHolder(View itemView) {
            super(itemView);

            pedidoTitulo = itemView.findViewById(R.id.pedidoTituloForn);
            pedidoQuantidade = itemView.findViewById(R.id.qtdTotalForn);
            pedidoData = itemView.findViewById(R.id.pedidoDataForn);
        }
    }
}
