package com.supermarket.projeto.classes.DAO;

import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.supermarket.projeto.classes.Activity.MainActivity;
import com.supermarket.projeto.classes.Activity.PrincipalActivity;

public class ConfigFirebase {

    private static DatabaseReference referenceFirebase;
    private static FirebaseAuth autenticacao;
    private static FirebaseStorage firebaseStorage;
    private static StorageReference storageReference;


    public static DatabaseReference getReferenceFirebase(){
        if (referenceFirebase == null){
            referenceFirebase = FirebaseDatabase.getInstance().getReference("usuarios");
        }
        return referenceFirebase;
    }

    public static FirebaseAuth getAutenticacao(){
        if (autenticacao == null){
            autenticacao = FirebaseAuth.getInstance();
        }

        return autenticacao;
    }

    public static FirebaseStorage getFirebaseStorage(){
        if (firebaseStorage == null){

            firebaseStorage = FirebaseStorage.getInstance();
        }

        return firebaseStorage;
    }

    public static StorageReference getStorageReference(){
        if (storageReference == null){
            storageReference = FirebaseStorage.getInstance().getReference();

        }

        return  storageReference;
    }


    public void desconectarUsuario() {
        autenticacao.signOut();
//        Intent intent = new Intent();
//        startActivity(intent);

    }
}
