package com.supermarket.projeto.classes.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Model.Produto;

import java.util.ArrayList;
import java.util.List;

public class ProdutoAdapter extends RecyclerView.Adapter<ProdutoAdapter.ViewHolder> {
    private List<Produto> mProdutoList;
    private Context context;
    private DatabaseReference databaseReference;
    private List<Produto> produtos;
    private Produto todosProdutos;


    public ProdutoAdapter(List<Produto> mProdutoList, Context context) {
        this.mProdutoList = mProdutoList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProdutoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_card_produto, viewGroup, false);

        return new ProdutoAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProdutoAdapter.ViewHolder holder, int position) {

        final Produto item = mProdutoList.get(position);

        produtos = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("produtos").orderByChild("keyProduto").equalTo(item.getKeyProduto()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                produtos.clear();


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    todosProdutos = postSnapshot.getValue(Produto.class);

                    produtos.add(todosProdutos);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        holder.textTitulo.setText(String.format("Produto: %s", item.getTitulo()));
//        holder.textDescricao.setText(String.format("Descrição: %s", item.getDescricao()));
//        holder.textCategoria.setText(String.format("Categoria: %s",item.getCategoria()));
//        holder.textMarca.setText(String.format("Marca: %s", item.getMarca()));
//        holder.textFornecedor.setText(new StringBuilder().append("Fornecedor: ").append(item.getFornecedor()).toString());
        holder.textValor.setText(String.format("R$ %s", item.getValor()));

    }

    @Override
    public int getItemCount() {
        return mProdutoList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
            protected TextView textTitulo;
            protected TextView textDescricao;
            protected TextView textCategoria;
            protected TextView textMarca;
            protected TextView textFornecedor;
            protected TextView textValor;
           // protected LinearLayout linearLayout;


        public ViewHolder(View itemView) {
            super(itemView);

            textTitulo = (TextView) itemView.findViewById(R.id.card_text_titulo);
//            textDescricao = (TextView) itemView.findViewById(R.id.card_text_descricao);
//            textCategoria = (TextView) itemView.findViewById(R.id.card_text_categoria);
//            textMarca =      (TextView) itemView.findViewById(R.id.card_text_marca);
//            textFornecedor = (TextView) itemView.findViewById(R.id.card_text_fornecedor);
            textValor =      (TextView) itemView.findViewById(R.id.card_view_valor);
            //linearLayout = (LinearLayout) itemView.findViewById()


        }
    }
}
