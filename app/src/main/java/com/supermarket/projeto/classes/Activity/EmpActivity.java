package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Interfaces.MetodosItemMenus;

public class EmpActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , MetodosItemMenus{
    FirebaseAuth autenticacao;
    Toolbar toolbar;
//    CardView cardViewAddUser;
//    CardView cardViewAddProd;
    CardView cardViewlistProd;
    CardView cardViewPedido;
    CardView cardViewAvisos;
    private final int EMP_USER = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp);
        toolbar = (Toolbar) findViewById(R.id.toolbarEmp);
        setSupportActionBar(toolbar);

        autenticacao = FirebaseAuth.getInstance();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        cardViewAddProd = (CardView) findViewById(R.id.cardCadastrarProdutoEmp);
//        cardViewAddUser = (CardView) findViewById(R.id.cardCadastrarUserEmp);
        cardViewlistProd = (CardView) findViewById(R.id.cardListarProdutosEmp);
        cardViewPedido = (CardView) findViewById(R.id.cardPedidosEmp);
        cardViewAvisos = (CardView) findViewById(R.id.cardAvisosEmp);

//        cardViewAddProd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cadastrarProduto();
//            }
//        });
//
//        cardViewAddUser.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        cardViewlistProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listProdutos();
            }
        });

        cardViewPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listarPedidos();

            }
        });

        cardViewAvisos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.emp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_addProdutoEmp) {
//
//
//
//        } else if (id == R.id.nav_addUsuarioEmp) {
//
//
//
//        } else
        if (id == R.id.nav_pedidosEmp) {

            Toast.makeText(
                    this, "Pedidos", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_listProdutosEmp) {

            listProdutos();

        } else if (id == R.id.nav_sairEmp) {

            desconectarUsuario();

        } else if (id == R.id.nav_avisosEmp) {


        }else if (id == R.id.nav_inforUserEmp) {

            mostrarDadosPerfil();

        }else if (id == R.id.nav_sendEmp) {

            Toast.makeText(this, "Enviar e-mail", Toast.LENGTH_LONG).show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void cadastrarProduto(){
//        Intent intent = new Intent(EmpActivity.this, CadastrarProdutoActivity.class);
//        startActivity(intent);
//
//    }


    @Override
    public void desconectarUsuario() {
        autenticacao.signOut();
        Intent intent = new Intent(EmpActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void cadastrarUsuarios() {

    }

    @Override
    public void uploadFotoPerfil() {
        Intent intent = new Intent(EmpActivity.this, UploadImagemActivity.class);
        startActivity(intent);


    }

    @Override
    public void mostrarDadosPerfil(){

        String activity = FornActivity.class.getSimpleName();
        Intent intent = new Intent(EmpActivity.this, PerfilUsuarioActivity.class);
        Bundle dados = new Bundle();
        dados.putString("atv", activity);

        intent.putExtras(dados);
        startActivity(intent);

    }

    private void listProdutos(){
        //listar produtor com a Classe ListProdutosActivity
        Intent intent = new Intent(EmpActivity.this, ListProdutosActivity.class);
        startActivity(intent);

    }

    private void listarPedidos(){

        Intent intent = new Intent(EmpActivity.this, ListarPedidosActivity.class);
        Bundle dados = new Bundle();
        dados.putInt("user", EMP_USER);
        intent.putExtras(dados);
        startActivity(intent);
    }

    public void messenger(String s){
        Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
    }
}
