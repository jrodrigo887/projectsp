package com.supermarket.projeto.classes.Activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.FragmentsProduto.InformacoesProdutosFragment;
import com.supermarket.projeto.classes.FragmentsProduto.VisaoGeralProdutosFragment;
import com.supermarket.projeto.classes.Model.Produto;

import org.parceler.Parcels;

public class DescricaoProdutosActivity extends AppCompatActivity implements VisaoGeralProdutosFragment.ListenerFragVisao {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private SmartTabLayout smartTabLayout;
    private String strTitulo="", strFornecedor="", strMarca="", strModelo="", strDescricao="", strValor="", strValidar="";
    VisaoGeralProdutosFragment fragOverView;
    InformacoesProdutosFragment fragInforView;
    Produto produto;
    Produto produtoFragView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descricao_produtos);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpagerProd);
        smartTabLayout = (SmartTabLayout) findViewById(R.id.viewPagerTabProd);

        fragOverView= new VisaoGeralProdutosFragment();
        fragInforView = new InformacoesProdutosFragment();
        getDadosProdutoList();

        //Enviar produto para fragment
        VisaoGeralProdutosFragment visFrag = new VisaoGeralProdutosFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putParcelable("pdt", Parcels.wrap(produto));
        visFrag.setArguments(bundle1);

        //configurar Abas
        FragmentPagerItemAdapter adapterPager = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.str_visaogeralpod, VisaoGeralProdutosFragment.class)
                .add(R.string.str_descricaopod, InformacoesProdutosFragment.class)
                .create()
        );

        viewPager.setAdapter(adapterPager);
        smartTabLayout.setViewPager(viewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_produto, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            //retorna para a tela anterior.
            finish();
            return true;
        }

        switch(id){
            case R.id.menuCarrinho:
                Toast.makeText(getApplicationContext(), "Carrinho", Toast.LENGTH_LONG).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    // Recebendo dados do produto clicado no recyclerView
    private void getDadosProdutoList(){

        final Produto pdt = (Produto) Parcels.unwrap(getIntent().getParcelableExtra("parcel"));
        produtoFragView = pdt;
        this.produto = pdt;
        getSupportActionBar().setTitle(pdt.getTitulo().toString());

        VisaoGeralProdutosFragment visaofrag = new VisaoGeralProdutosFragment();
        Bundle fragBundle = new Bundle();

//            fragBundle.putString("valor", pdt.getValor().toString());
//            visaofrag.setArguments(fragBundle);

    }

    public Produto getDataProduto() {

        return produto;
    }


    @Override
    public void setOnFragVisao() {



    }


}
