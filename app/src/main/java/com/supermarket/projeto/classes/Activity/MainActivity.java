package com.supermarket.projeto.classes.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.beardedhen.androidbootstrap.api.view.BootstrapTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Helper.Preferencias;
import com.supermarket.projeto.classes.Model.Usuario;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth autentic;
    private BootstrapEditText edtEmail, edtLogin;
    private BootstrapButton btnLogin;
    private Usuario usuario;
    DatabaseReference databaseReference;
    private String tipoUsuarioEmail;
    private Preferencias preferencias;
    private TextView esqueciSenha;
    private Dialog dialog;
    private AlertDialog.Builder alertDialog;
    private TextInputLayout idTIemail;
    private TextInputLayout idTIsenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = new Usuario();
        edtEmail = (BootstrapEditText) findViewById(R.id.idETgetEmailLogin);
        edtLogin = (BootstrapEditText) findViewById(R.id.idEditgetSenhaLogin);
        btnLogin = (BootstrapButton) findViewById(R.id.idButtonLogin);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        esqueciSenha = (TextView) findViewById(R.id.txtEsqueciSenha);
        idTIemail = (TextInputLayout) findViewById(R.id.idTIemail);
        idTIsenha = (TextInputLayout) findViewById(R.id.idTIsenhaLogin);


        final EditText editTextEmail = new EditText(MainActivity.this);
        editTextEmail.setHint("exempo@email.com");

        if (userLog()){
            abrirTelaPrincipal();


        }else{          //Botão login

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!edtEmail.getText().toString().equals("") && !edtLogin.getText().toString().equals("")){

                        usuario.setEmail(edtEmail.getText().toString());
                        usuario.setSenha(edtLogin.getText().toString());
                        validarLogin();

                    }else {

                        if(edtEmail.getText().toString().isEmpty()){
                            idTIemail.setEnabled(true);
                            idTIemail.setError("Preencha com seu Email");


                        }else{
                            idTIsenha.setEnabled(false);
                        }


                        if (edtLogin.getText().toString().isEmpty()){
                            idTIsenha.setEnabled(true);
                            idTIsenha.setError("Preencha com sua senha");
                        }else{
                            idTIsenha.setEnabled(false);

                        }

                      //  Toast.makeText(getApplicationContext(),"Preencha os campos E-mail e Senha", Toast.LENGTH_LONG).show();

                    }
                }
            });

        }


        esqueciSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setCancelable(false);
                alertDialog.setTitle("Recuperar Senha");
                alertDialog.setMessage("Informe o seu e-mail!");
                alertDialog.setView(editTextEmail);
                editTextEmail.setText((CharSequence) edtEmail.getText().toString(), TextView.BufferType.EDITABLE);


                if (!editTextEmail.getText().equals("")){

                    alertDialog.setPositiveButton("Recuperar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            autentic = FirebaseAuth.getInstance();


                            String emailRecuperar = editTextEmail.getText().toString();

                            //Resetar a senha com identificação de e-mail do usuário.
                            autentic.sendPasswordResetEmail(emailRecuperar).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        messenger("Em instantes você receberar um E-mail!");

                                        //Pegando a activity atual para fazer o reset, evitando possível erro posterior do AlertDialog.
                                        resetItent();

                                    } else {

                                        messenger("Falha ao enviar o E-mail!");

                                        //Pegando a activity atual para fazer o reset, evitando possível erro posterior do AlertDialog.
                                        resetItent();

                                    }


                                }
                            });


                        }
                    });

                    alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            resetItent();

                        }
                    });

                }else{

                    messenger("Preencha o Campo de E-mail");

                }

                alertDialog.create();
                alertDialog.show();

            }
        });

    }
    //Autenticação com Firebase
    private void validarLogin(){
        autentic = ConfigFirebase.getAutenticacao();
        autentic.signInWithEmailAndPassword(usuario.getEmail(), usuario.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()){

                    preferencias = new Preferencias(MainActivity.this);
                    preferencias.salvarUsuarioPreferencias(usuario.getEmail(), usuario.getSenha());
                    abrirTelaPrincipal();

                    Toast.makeText(getApplicationContext(),"Login efetuado com Sucesso! " + preferencias.getEmailUsuarioLogado(), Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(getApplicationContext(),"", Toast.LENGTH_LONG).show();

                    messenger("Usuário ou Senha Incorreto");
                }
            }
        });


    }

    //Intent da tela padrão principal.
    private void abrirTelaPrincipal(){
        String email;
        //Puxar os dados do banco para verificar o tipo de usuário ativo.

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        email = firebaseAuth.getCurrentUser().getEmail();


        databaseReference.child("usuarios").child("usuarios").orderByChild("email").equalTo(email).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    if (postSnapshot.child("tipoUsuario").getValue()!=null){
                        //Buscando o tipo de usuário.
                        tipoUsuarioEmail = postSnapshot.child("tipoUsuario").getValue().toString();
                       // Log.d("principal", "Tipo usuario" + tipoUsuarioEmail);

                        //Verificar o tipo de usuário e em seguida instaciar activity correspondente.

                        tipoUsuarioIntent(tipoUsuarioEmail);
//                        if (tipoUsuarioEmail.equals("Administrador")){
//
//                            Intent intent = new Intent(MainActivity.this, AdminActivity.class);
//                            finish();
//                            startActivity(intent);
//
//                        }else if (tipoUsuarioEmail.equals("Fornecedor")){
//
//                            Intent intent = new Intent(MainActivity.this, FornActivity.class);
//                            finish();
//                            startActivity(intent);
//
//                        }else if (tipoUsuarioEmail.equals("Empresa")){
//
//                            Intent intent = new Intent(MainActivity.this, EmpActivity.class);
//                            finish();
//                            startActivity(intent);
//
//                        }

                    }else{
                        messenger("Tipo de usuário não identificado");

                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    //Verificar se o usuário está instanciado ou não;
    public boolean userLog(){
        FirebaseUser user;
        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {

            return true;

        }else{
            return false;
        }

    }

    public boolean tipoUsuarioIntent(String tipoUserEmail){

        if (tipoUserEmail.equals("Administrador")){

            Intent intent = new Intent(MainActivity.this, AdminActivity.class);
            startActivity(intent);
            finish();
            return true;

        }else if (tipoUserEmail.equals("Fornecedor")){

            Intent intent = new Intent(MainActivity.this, FornActivity.class);
            startActivity(intent);
            finish();
            return true;

        }else if (tipoUserEmail.equals("Empresa")){

            Intent intent = new Intent(MainActivity.this, EmpActivity.class);
            startActivity(intent);
            finish();
            return true;
        }else{

            return false;

        }
    }

    public void iniciarAtividade(Intent intent){
        startActivity(intent);
    }

    private void resetItent(){
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void messenger(String s){
        String msn = s;

        Toast.makeText(MainActivity.this, msn, Toast.LENGTH_LONG).show();
    }

}
