package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Adapter.PedidoAdapterEmp;
import com.supermarket.projeto.classes.Adapter.PedidoAdapterForn;
import com.supermarket.projeto.classes.Adapter.PedidoAdapterRecycler;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Helper.RecyclerItemClickListener;
import com.supermarket.projeto.classes.Model.Pedidos;

import java.util.ArrayList;

public class ListarPedidosActivity extends AppCompatActivity {

    Toolbar toolbar;
    private RecyclerView mRecyclerViewPedidos;

    private PedidoAdapterRecycler adapterPedAdmin;

    private PedidoAdapterEmp adapterEmp;

    private PedidoAdapterForn adapterForn;

    private ArrayList<Pedidos> pedidosArray;

    private Pedidos pediido;

    private DatabaseReference referenciaFirebase;
    private FirebaseAuth autenticacao;
    private FirebaseUser firebaseUser;
    private String userLogado;
    int tipoUse = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_pedidos_admin);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setTitle("Pedidos");

        mRecyclerViewPedidos = (RecyclerView) findViewById(R.id.recyclerPedidos);


        referenciaFirebase = ConfigFirebase.getReferenceFirebase();
        autenticacao = FirebaseAuth.getInstance();
        userLogado = autenticacao.getCurrentUser().getEmail();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        tipoUse = bundle.getInt("user");

        //Setando Layout do RecyclerView
        mRecyclerViewPedidos.setHasFixedSize(true);
        mRecyclerViewPedidos.setLayoutManager(new LinearLayoutManager(this));

        pedidosArray = new ArrayList<>();


        //1 para admi, 2 - para Fornecedor e 3 - para Empresas.
        if (tipoUse == 1){
            //Carregar para Admin
            carregarPedidos();
            getSupportActionBar().setTitle("Administrador");

        }else if (tipoUse == 2){

            pedidosFornecedor();
            getSupportActionBar().setTitle("Fornecedor");
        }else if (tipoUse == 3){

            pedidosEmpresa();
            getSupportActionBar().setTitle("Empresa");
        }else{

            Toast.makeText(getApplicationContext(), "Erro de acesso aos pedidos", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            //retorna para a tela anterior.
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void carregarPedidos(){

        Log.i("admin","Chegou ao metodo carregarPedidos");

        referenciaFirebase.child("pedidos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    pediido = postSnapshot.getValue(Pedidos.class);

                    Log.d("teste", "Teste da lista de pedidos Admin");

                    pedidosArray.add(pediido);

                }


                adapterPedAdmin.notifyDataSetChanged();
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        adapterPedAdmin = new PedidoAdapterRecycler( this, pedidosArray);
        mRecyclerViewPedidos.setAdapter(adapterPedAdmin);

        mRecyclerViewPedidos.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(),
                mRecyclerViewPedidos,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                }));


    }


    public void pedidosEmpresa(){

        Log.i("forn","Chegou ao metodo carregarPedidos Fornecedor");

        referenciaFirebase.child("pedidos").orderByChild("emailEmpresa").equalTo(userLogado)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            pediido = postSnapshot.getValue(Pedidos.class);

                            Log.d("teste", "Recycler Empresa");

                            pedidosArray.add(pediido);

                        }

                        adapterEmp.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        adapterEmp = new PedidoAdapterEmp( this, pedidosArray);
        mRecyclerViewPedidos.setAdapter(adapterEmp);


    }
    public void pedidosFornecedor(){

        Log.i("teste","Chegou ao metodo carregarPedidos Fornecedor");

        referenciaFirebase.child("pedidos").orderByChild("fornecedor").equalTo(userLogado)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            pediido = postSnapshot.getValue(Pedidos.class);

                            Log.d("admin", "Teste da lista de pedidos");

                            pedidosArray.add(pediido);

                        }

                        adapterForn.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

        adapterForn = new PedidoAdapterForn( this, pedidosArray);
        mRecyclerViewPedidos.setAdapter(adapterForn);
    }
}


