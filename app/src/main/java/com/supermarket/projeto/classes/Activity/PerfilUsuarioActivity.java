package com.supermarket.projeto.classes.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Model.Usuario;

public class PerfilUsuarioActivity extends AppCompatActivity {
    Toolbar toolbar;

    private TextView txtNome;
    private TextView txtEmail;
    private TextView txtTipoUser;
    private TextView txtCpf;
    private TextView txtCelular;
    private TextView txtRua;
    private TextView txtNum;
    private TextView txtComplemento;
    private TextView txtCep;
    private TextView txtBairro;
    private TextView txtCidade;
    private TextView txtEstado;

    private BootstrapButton btnEditarPerfil;
    private BootstrapButton btnCancelPerfil;
    private BootstrapButton btnExcluirPerfil;
    private FirebaseAuth autenticacao;
    private DatabaseReference reference;
    private String activityAnterior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Dados usuário
        txtNome = (TextView) findViewById(R.id.perfilNome);
        txtEmail = (TextView) findViewById(R.id.perfilEmail);
        txtTipoUser = (TextView) findViewById(R.id.perfilTipoUser);
        txtCpf = (TextView) findViewById(R.id.perfilCpf);
        txtCelular = (TextView) findViewById(R.id.perfilCelular);
        //Atributos de Endereço
        txtRua = (TextView) findViewById(R.id.perfilEndRua);
        txtNum = (TextView) findViewById(R.id.perfilEndNumero);
        txtComplemento = (TextView) findViewById(R.id.perfilEndComplemento);
        txtCep = (TextView) findViewById(R.id.perfilEndCep);
        txtBairro = (TextView) findViewById(R.id.perfilEndBairro);
        txtCidade = (TextView) findViewById(R.id.perfilEndCidade);
        txtEstado = (TextView) findViewById(R.id.perfilEndEstado);

        btnEditarPerfil = (BootstrapButton) findViewById(R.id.btnEditarPerfil);
        btnCancelPerfil = (BootstrapButton) findViewById(R.id.btnCancelPerfil);
        btnExcluirPerfil = (BootstrapButton) findViewById(R.id.btnPerfilExlcuir);

        autenticacao = FirebaseAuth.getInstance();

        reference = ConfigFirebase.getReferenceFirebase();

        //recuperando o nome da activity anterior
//        Intent intent = getIntent();
//        Bundle dados = intent.getExtras();
//         activityAnterior = dados.getString("atv");



        String usuarioLogado = autenticacao.getCurrentUser().getEmail();

        reference.child("usuarios").orderByChild("email").equalTo(usuarioLogado).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    Usuario user = postSnapshot.getValue(Usuario.class);

                    txtNome.setText(String.format("Nome: %s", user.getNome()));
                    txtEmail.setText(String.format("E-mail: %s", user.getEmail()));
                    txtTipoUser.setText(String.format("Tipo Usuário: %s", user.getTipoUsuario()));
                    txtCpf.setText(String.format("CPF: %s", user.getCnpj()));
                    txtCelular.setText(String.format("Celular: %s", user.getCelular()));
                    //Endereço
                    txtRua.setText(String.format("Rua: %s", user.getEndereco().getRua()));
                    txtNum.setText(String.format("Número: %s", user.getEndereco().getNumero()));
                    txtComplemento.setText(String.format("Complemento: %s", user.getEndereco().getComplemeto()));
                    txtCep.setText(String.format("CEP: %s", user.getEndereco().getCep()));
                    txtBairro.setText(String.format("Bairro: %s", user.getEndereco().getBairro()));
                    txtCidade.setText(String.format("Cidade: %s", user.getEndereco().getCidade()));
                    txtEstado.setText(String.format("UF: %s", user.getEndereco().getUf()));


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btnCancelPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelarPerfil();
            }
        });

        btnExcluirPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaExcluir();
            }
        });

        btnEditarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editarPerfil();

            }
        });




    }



    private void editarPerfil(){

        String emailUserLogado = autenticacao.getCurrentUser().getEmail();

        reference = ConfigFirebase.getReferenceFirebase();

        reference.child("usuarios").orderByChild("email").equalTo(emailUserLogado).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapShot : dataSnapshot.getChildren()){
                   Usuario usuario = postSnapShot.getValue(Usuario.class);

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    //Enviar os dados via bundle para editar perfil.
                    final  Intent intent = new Intent(PerfilUsuarioActivity.this, EditarPerfilActivity.class);
                    final Bundle bundle = new Bundle();
                    bundle.putString("inicio", "dadosEditar");
                    bundle.putString("nome", usuario.getNome());
                    bundle.putString("cpf", usuario.getCnpj());
                    bundle.putString("chave", usuario.getKeyUsuario());
                    bundle.putString("email", usuario.getEmail());
                    bundle.putString("tipoUsuario", usuario.getTipoUsuario());
                    bundle.putString("email", usuario.getEmail());
                    bundle.putString("telefone", usuario.getTelefone());
                    bundle.putString("celular", usuario.getCelular());
                    bundle.putString("rua", usuario.getEndereco().getRua());
                    bundle.putString("numero", usuario.getEndereco().getNumero());
                    bundle.putString("cep", usuario.getEndereco().getCep());
                    bundle.putString("bairro", usuario.getEndereco().getBairro());
                    bundle.putString("cidade", usuario.getEndereco().getCidade());
                    bundle.putString("estado", usuario.getEndereco().getUf());
                    bundle.putString("complemento", usuario.getEndereco().getComplemeto());

                    intent.putExtras(bundle);
                    startActivity(intent);



                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void cancelarPerfil(){
        txtNome.setText("");
        txtEmail.setText("");
        txtTipoUser.setText("");

        returnActivity();
    }

    private void excluirperfilDeslogar(){
        String emailUserLogado = autenticacao.getCurrentUser().getEmail();

        reference = ConfigFirebase.getReferenceFirebase();

        reference.child("usuarios").orderByChild("email").equalTo(emailUserLogado).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapShot : dataSnapshot.getChildren()){
                    final Usuario usuario = postSnapShot.getValue(Usuario.class);

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


                    //Deletando perfil local
                    user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful()){
                                Log.d("perfil", "User deletado");

                                Toast.makeText(PerfilUsuarioActivity.this,"Perfil deletado com sucesso!", Toast.LENGTH_LONG).show();

                                //Comando para deletar perfil no firebase
                                reference = ConfigFirebase.getReferenceFirebase();
                                reference.child("usuarios").child(usuario.getKeyUsuario()).removeValue();

                                autenticacao.signOut();
                                abrirTelaPrincipal();

                            }
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void returnActivity(){
//        if (activityAnterior.equals("AdministradorActivity")){
//            Intent intent = new Intent(PerfilUsuarioActivity.this, AdministradorActivity.class);
//            startActivity(intent);
//        }else if (activityAnterior.equals("EmpActivity")){
//            Intent intent = new Intent(PerfilUsuarioActivity.this, EmpActivity.class);
//            startActivity(intent);
//
//        }else if (activityAnterior.equals("FornecedorActivity")){
//            Intent intent = new Intent(PerfilUsuarioActivity.this, FornecedorActivity.class);
//            startActivity(intent);
//        }

        Intent intent = new Intent(PerfilUsuarioActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void abrirTelaPrincipal(){
        Intent intent = new Intent(PerfilUsuarioActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void abrirTelaExcluir(){
        final Dialog dialog = new Dialog(PerfilUsuarioActivity.this);

        dialog.setContentView(R.layout.alert_personalizado_excluir);

        final BootstrapButton btnSim = (BootstrapButton) dialog.findViewById(R.id.btnSim);
        final BootstrapButton btnNao = (BootstrapButton) dialog.findViewById(R.id.btnNao);

        btnSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                excluirperfilDeslogar();
                dialog.dismiss();
               // Toast.makeText(PerfilUsuarioActivity.this, "Usuário Excluído com Sucesso!", Toast.LENGTH_LONG).show();

            }
        });

        btnNao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaPrincipal();
                dialog.dismiss();

            }
        });

        dialog.show();
    }


}
