package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Adapter.ProdutoAdapter;
import com.supermarket.projeto.classes.Helper.RecyclerItemClickListener;
import com.supermarket.projeto.classes.Model.Produto;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class ListProdutosActivity extends AppCompatActivity {
    private Toolbar toolbar;

    private RecyclerView mRecyclerViewProdutos;

    private ProdutoAdapter adapter ;

    private List<Produto> produtos;

    private DatabaseReference referenciaFirebase;

    private Produto todosProdutos;

    private LinearLayoutManager mLayoutManagerTodosProdutos;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produtos);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerViewProdutos = (RecyclerView) findViewById(R.id.recycler_list_pdt);


        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("Produtos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        carregarTodosProdutos();
    }

    private void carregarTodosProdutos() {

        mRecyclerViewProdutos.setHasFixedSize(true);

        Configuration orientation = new Configuration();
        if(this.mRecyclerViewProdutos.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mRecyclerViewProdutos.setLayoutManager(new GridLayoutManager(this, 2));
        } else if (this.mRecyclerViewProdutos.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mRecyclerViewProdutos.setLayoutManager(new GridLayoutManager(this, 4));
        }

//        mLayoutManagerTodosProdutos = new LinearLayoutManager(this, GridLayoutManager.VERTICAL, false);
//
//        mRecyclerViewProdutos.setLayoutManager(mLayoutManagerTodosProdutos);

        produtos = new ArrayList<>();

        referenciaFirebase = FirebaseDatabase.getInstance().getReference();


        referenciaFirebase.child("usuarios").child("produtos").orderByChild("fornecedor").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    todosProdutos = postSnapshot.getValue(Produto.class);
                    String forn = todosProdutos.getFornecedor().toString();
                    Log.d("teste", "Teste da lista de produtos");

                    produtos.add(todosProdutos);

                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        adapter = new ProdutoAdapter(produtos, this);

        mRecyclerViewProdutos.setAdapter(adapter);

        mRecyclerViewProdutos.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        mRecyclerViewProdutos,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Produto pdt = produtos.get(position);

                                overViewProduto(pdt);
                                Toast.makeText(getApplicationContext(),"Click curto" + pdt.getTitulo(), Toast.LENGTH_LONG ).show();

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                              Produto pdt = produtos.get(position);

                                overViewProduto(pdt);
                                Toast.makeText(getApplicationContext(),"Click Longo" + pdt.getTitulo(), Toast.LENGTH_LONG ).show();
                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Produto pdt = produtos.get(position);

                                overViewProduto(pdt);

                            }
                        }
                )
        );


    }

    private void overViewProduto( Produto pdt){

        final Intent intent = new Intent(ListProdutosActivity.this, DescricaoProdutosActivity.class);

        intent.putExtra("parcel", Parcels.wrap(pdt));
        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            //retorna para a tela anterior.
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}