package com.supermarket.projeto.classes.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;

import java.io.ByteArrayOutputStream;

public class UploadImagemActivity extends AppCompatActivity {


    private BootstrapButton btnUpload;
    private BootstrapButton btnCancelar;
    private StorageReference storageReference;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private FirebaseAuth autenticacao;
    private ImageView imageView;
    private String emailUsuarioLogado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_imagem);

        imageView = (ImageView) findViewById(R.id.imagemView);

        storageReference = ConfigFirebase.getStorageReference();
        autenticacao = ConfigFirebase.getAutenticacao();

        //Solicitar permissão de escrita e leitura no dispositivo.
 //       permission();
        emailUsuarioLogado = autenticacao.getCurrentUser().getEmail();

        carregaImagemPadrao();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "Selecione uma Imagem!"), 123);


            }
        });

        btnUpload = (BootstrapButton) findViewById(R.id.btnUpload);
        btnCancelar = (BootstrapButton) findViewById(R.id.btnCancelarUpload);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastroFotoPerfil();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UploadImagemActivity.this, PrincipalActivity.class);
                finish();
                startActivity(intent);
            }
        });

    }


    //Buscar imagem padrão no Storege Firebase
    private void carregaImagemPadrao(){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        final StorageReference storageReference = storage.getReferenceFromUrl(
                "gs://supermarketbrasil-b07ba.appspot.com/fotoPerfilUsuario/" + emailUsuarioLogado + ".jpg");

        final int height = 400;
        final int width = 400;

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).resize(width, height).centerCrop().into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

    private void cadastroFotoPerfil(){
        StorageReference montarImgReferencia= storageReference.child("fotoPerfilUsuario/" + emailUsuarioLogado+ ".jpg");
        imageView.setDrawingCacheEnabled(true);
        imageView.buildDrawingCache();

        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100 ,byteArray);
        byte [] data = byteArray.toByteArray();

        UploadTask uploadTask = montarImgReferencia.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getUploadSessionUri();
                carregaImagemPadrao();

                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final int height = 400;
        final int width = 400;

        if (resultCode == Activity.RESULT_OK){
            if (requestCode == 123) {
                Uri imagemSelecionada = data.getData();
                Picasso.get().load(imagemSelecionada.toString()).resize(width, height).centerCrop().into(imageView);


            }
        }
    }

    public void permission(){
        int PERMISSION_ALL = 1;
        //checando a permissões do usuário.
//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//        if (permissionCheck == -1){
//
//            shouldShowRequestPermissionRationale(this, );
//        }else if (permissionCheck == 0 ){
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSION_ALL );
//        }

        String [] PERMISSION = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this, PERMISSION, PERMISSION_ALL);


    }
}
