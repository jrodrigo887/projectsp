package com.supermarket.projeto.classes.Model;

public class Pedidos {

     private String empresaKey;
     private String emailEmpresa;
     private String nomeEmpresa;
     private String dateAndTime;
     private String nomeProduto;
     private String produtokey;
     private String valor;
     private String quantidade;
     private String pedidoKey;
     private String fornecedor;
     private String situacao;



    public Pedidos(String empresaKey, String emailEmpresa, String nomeEmpresa, String dateAndTime, String nomeProduto, String produtokey, String valor, String quantidade, String situacao) {
        this.empresaKey = empresaKey;
        this.emailEmpresa = emailEmpresa;
        this.nomeEmpresa = nomeEmpresa;
        this.dateAndTime = dateAndTime;
        this.nomeProduto = nomeProduto;
        this.produtokey = produtokey;
        this.situacao = situacao;
        this.valor = valor;
        this.quantidade = quantidade;
    }

    public Pedidos() {

    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getEmailEmpresa() {
        return emailEmpresa;
    }

    public void setEmailEmpresa(String emailEmpresa) {
        this.emailEmpresa = emailEmpresa;
    }

    public String getEmpresaKey() {
        return empresaKey;
    }

    public void setEmpresaKey(String empresaKey) {
        this.empresaKey = empresaKey;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getProdutokey() {
        return produtokey;
    }

    public void setProdutokey(String produtokey) {
        this.produtokey = produtokey;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }

    public String getPedidoKey() {
        return pedidoKey;
    }

    public void setPedidoKey(String pedidoKey) {
        this.pedidoKey = pedidoKey;
    }

}
