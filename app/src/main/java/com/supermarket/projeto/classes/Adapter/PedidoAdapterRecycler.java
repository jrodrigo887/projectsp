package com.supermarket.projeto.classes.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Model.Pedidos;

import java.util.ArrayList;
import java.util.List;

public class PedidoAdapterRecycler extends RecyclerView.Adapter<PedidoAdapterRecycler.ViewHolder> {
    private Context context;
    private ArrayList<Pedidos> mylistPedidos;
    private Pedidos allRequest;
    private List<Pedidos> listPedidos;
    DatabaseReference databaseReference;

    public PedidoAdapterRecycler(Context context, ArrayList<Pedidos> mylistPedidos) {
        this.context = context;
        this.mylistPedidos = mylistPedidos;
    }

    @NonNull
    @Override
    public PedidoAdapterRecycler.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_pedido_admin, viewGroup, false);

        return new PedidoAdapterRecycler.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PedidoAdapterRecycler.ViewHolder holder, int position) {

        final Pedidos item = mylistPedidos.get(position);

        listPedidos = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("produtos").orderByChild("keyProduto").equalTo(item.getPedidoKey()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                listPedidos.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    allRequest = postSnapshot.getValue(Pedidos.class);
                   listPedidos.add(allRequest);

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        holder.pedidoEmpresa.setText(item.getEmailEmpresa());
        holder.pedidoTitulo.setText(item.getNomeProduto());
        holder.pedidoFornecedor.setText(item.getFornecedor());
        holder.pedidoPreco.setText(item.getValor());
        holder.pedidoQuantidade.setText(item.getQuantidade());

    }

    @Override
    public int getItemCount() {
        return mylistPedidos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView pedidoTitulo;
        private TextView pedidoPreco;
        private TextView pedidoQuantidade;
        private TextView pedidoTotal;
        private TextView pedidoFornecedor;
        private TextView pedidoEmpresa;

        public ViewHolder(View itemView) {
            super(itemView);

            pedidoTitulo = itemView.findViewById(R.id.pedidoTituloAdmin);
            pedidoPreco = itemView.findViewById(R.id.pedidoPrecoAdmin);
            pedidoQuantidade = itemView.findViewById(R.id.pedidoQtdAdmin);
            pedidoTotal = itemView.findViewById(R.id.pedidoTotalAdmin);
            pedidoFornecedor = itemView.findViewById(R.id.pedidoFornAdmin);
            pedidoEmpresa = itemView.findViewById(R.id.pedidoEmpAdmin);
        }
    }
}
