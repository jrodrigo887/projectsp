package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Model.Usuario;

public class AdminActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ConfigFirebase configFirebase;
    FirebaseAuth autenticacao;
    DatabaseReference referenceFirebase;
    private int ADMIN_USER = 1;
    CardView cardViewAddUser;
    CardView cardViewAddProd;
    CardView cardViewlistProd;
    CardView cardViewPedido;
    CardView cardViewAvisos;

    private TextView navEmail, navNome;
//    private ImageViewCompat navImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

          Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Administrador");

//        navEmail = (TextView) findViewById(R.id.nav_email_admin);
//        navNome =  (TextView) findViewById(R.id.nav_nome_admin);

        //ButtonFloating
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        cardViewAddProd = (CardView) findViewById(R.id.cardCadastrarProdutoAdmin);
        cardViewAddUser = (CardView) findViewById(R.id.cardCadastrarUserAdmin);
        cardViewlistProd = (CardView) findViewById(R.id.cardListarProdutos);
        cardViewPedido = (CardView) findViewById(R.id.cardPedidosAdmin);
        cardViewAvisos = (CardView) findViewById(R.id.cardAvisosAdmin);

        referenceFirebase = ConfigFirebase.getReferenceFirebase();
        autenticacao = FirebaseAuth.getInstance();
        if (autenticacao.getCurrentUser() != null){
            String email = autenticacao.getCurrentUser().getEmail();

            Log.i("admin", "E-mail " + email);



//            referenceFirebase.child("usuarios").orderByChild("email").equalTo(email).addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    Usuario user = new Usuario();
//
//
//                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//
//                        user = postSnapshot.getValue(Usuario.class);
//
//                        Log.d("teste", "Nome "+ user.getNome());
//                        Log.d("teste", "Nome "+ user.getEmail());
//
//                        // navNome.setText(user.getNome());
//                        //navEmail.setText(user.getEmail());
//
//                    }
//
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });

        }

        cardViewAddProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarProduto();
            }
        });

        cardViewAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarUsuarios();
            }
        });

        cardViewlistProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listProdutos();
            }
        });

        cardViewPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listarPedidos();
            }
        });

        cardViewAvisos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            desconectarUsuario();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_addProdutoAdmin) {

            cadastrarProduto();

        } else if (id == R.id.nav_addUsuarioAdmin) {

            cadastrarUsuarios();

        } else if (id == R.id.nav_pedidosAdmin) {

            Toast.makeText(
                    this, "Pedidos", Toast.LENGTH_LONG).show();


        } else if (id == R.id.nav_listProdutosAdmin) {


            listProdutos();

        } else if (id == R.id.nav_sairAdmin) {

            desconectarUsuario();

        } else if (id == R.id.nav_avisosAdmin) {

            desconectarUsuario();

        }else if (id == R.id.nav_inforUserAdmin) {

            mostrarDadosPerfil();

        }else if (id == R.id.nav_sendAdmin) {

            Toast.makeText(this, "Enviar e-mail", Toast.LENGTH_LONG).show();
            desconectarUsuario();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void desconectarUsuario() {

        autenticacao.signOut();
        Intent intent = new Intent(AdminActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void cadastrarUsuarios(){
        Intent intent = new Intent(AdminActivity.this, CadastroUsuarioActivity.class);
        startActivity(intent);
    }

    public void cadastrarProduto(){
        Intent intent = new Intent(AdminActivity.this, CadastrarProdutoActivity.class);
        startActivity(intent);
    }

    private void uploadFotoPerfil(){
        Intent intent = new Intent(AdminActivity.this, UploadImagemActivity.class);
        startActivity(intent);

    }

    private void listProdutos(){
        //listar produtor com a Classe ListProdutosActivity
        Intent intent = new Intent(AdminActivity.this, ListProdutosActivity.class);
        startActivity(intent);

    }

    private void mostrarDadosPerfil(){
        String activity = AdminActivity.class.getSimpleName();
        Intent intent = new Intent(AdminActivity.this, PerfilUsuarioActivity.class);
        Bundle dados = new Bundle();
        dados.putString("atv", activity);
        intent.putExtras(dados);
        startActivity(intent);

    }

    private void listarPedidos(){

        Intent intent = new Intent(AdminActivity.this, ListarPedidosActivity.class);
        Bundle dados = new Bundle();
        dados.putInt("user", ADMIN_USER);
        intent.putExtras(dados);
        startActivity(intent);
    }
}
