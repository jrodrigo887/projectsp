package com.supermarket.projeto.classes.Interfaces;

import android.content.Intent;

import static android.support.v4.content.ContextCompat.startActivity;

public interface MetodosItemMenus {


    void desconectarUsuario();
    void cadastrarUsuarios();
    void uploadFotoPerfil();
    void mostrarDadosPerfil();

}
