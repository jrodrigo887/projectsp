package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Interfaces.MetodosItemMenus;
import com.supermarket.projeto.classes.Model.Usuario;

public class PrincipalActivity extends AppCompatActivity {

    private FirebaseAuth autenticacao;
    private DatabaseReference referenceFirebase;
    private TextView tipoUsuario;
    private Usuario usuario;
    private String tipoUsuarioEmail;
    private String email;
    Menu menu1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        autenticacao = FirebaseAuth.getInstance();



        referenceFirebase = FirebaseDatabase.getInstance().getReference();
        if (autenticacao.getCurrentUser()!=null){

            email = autenticacao.getCurrentUser().getEmail();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        menu.clear();
        this.menu1 = menu;

        getMenuInflater().inflate(R.menu.menu_padrao, menu1);

        //Pegando e-mail do usuário logado no momento.
        // String email =  autenticacao.getCurrentUser().getEmail().toString();


//        referenceFirebase.child("usuarios").child("usuarios").orderByChild("email").equalTo(email).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
//                    if (postSnapshot.child("tipoUsuario").getValue()!=null){
//                        tipoUsuarioEmail = postSnapshot.child("tipoUsuario").getValue().toString();
//                        Log.d("principal", "Tipo usuario" + tipoUsuarioEmail);
//                        menu1.clear();
//
//                        if (tipoUsuarioEmail.equals("Administrador")){
//                            getMenuInflater().inflate(R.menu.menu_admin,menu1);
//                        }else if (tipoUsuarioEmail.equals("Fornecedor")){
//                            getMenuInflater().inflate(R.menu.menu_fornecedor,menu1);
//                        }else if (tipoUsuarioEmail.equals("Empresa")){
//                            getMenuInflater().inflate(R.menu.menu_empresa,menu1);
//                        }
//                        tipoUsuario.setText(tipoUsuarioEmail);
//
//
//
//                    }else{
//                        Toast.makeText(PrincipalActivity.this, "Tipo de usuário não identificado", Toast.LENGTH_LONG).show();
//                        tipoUsuario.setText("Usuário Padrão");
//                        getMenuInflater().inflate(R.menu.menu_padrao,menu1);
//
//                    }
//                }
//
//
//
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        int id = menuItem.getItemId();


        if (id == R.id.menuAddUser){


        }else if (id == R.id.menuActionSair){


        }else if (id == R.id.fotoPerfil){
            Intent intent = new Intent(PrincipalActivity.this, UploadImagemActivity.class);
            startActivity(intent);
        }
        return true;

    }

}
