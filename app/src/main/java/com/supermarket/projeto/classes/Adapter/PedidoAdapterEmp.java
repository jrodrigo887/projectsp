package com.supermarket.projeto.classes.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.Model.Pedidos;

import java.util.ArrayList;
import java.util.List;

public class PedidoAdapterEmp extends RecyclerView.Adapter<PedidoAdapterEmp.ViewHolder> {
    private Context context;
    private ArrayList<Pedidos> mylistPedidos;
    private Pedidos allRequest;
    private List<Pedidos> listPedidos;
    DatabaseReference databaseReference;

    public PedidoAdapterEmp(Context context, ArrayList<Pedidos> mylistPedidos) {
        this.context = context;
        this.mylistPedidos = mylistPedidos;
    }

    @NonNull
    @Override
    public PedidoAdapterEmp.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_pedido_emp, viewGroup, false);

        return new PedidoAdapterEmp.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PedidoAdapterEmp.ViewHolder holder, int position) {

        final Pedidos item = mylistPedidos.get(position);

        listPedidos = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

//        databaseReference.child("produtos").orderByChild("keyProduto").equalTo(item.getPedidoKey()).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                listPedidos.clear();
//
//
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
//
//                    allRequest = postSnapshot.getValue(Pedidos.class);
//
//                   listPedidos.add(allRequest);
//
//                }
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        holder.pedidoTitulo.setText(item.getNomeProduto());
        holder.pedidoFornecedor.setText(item.getFornecedor());
        holder.pedidoPreco.setText(item.getValor());
        holder.pedidoQuantidade.setText(item.getQuantidade());
        holder.pedidoSituacao.setText(item.getSituacao());



    }

    @Override
    public int getItemCount() {
        return mylistPedidos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView pedidoTitulo;
        private TextView pedidoPreco;
        private TextView pedidoQuantidade;
        private TextView pedidoTotal;
        private TextView pedidoFornecedor;
        private TextView pedidoSituacao;

        public ViewHolder(View itemView) {
            super(itemView);

            pedidoTitulo = itemView.findViewById(R.id.pedidoTituloEmp);
            pedidoPreco = itemView.findViewById(R.id.pedidoPrecoEmp);
            pedidoQuantidade = itemView.findViewById(R.id.pedidoQtdEmp);
            pedidoFornecedor = itemView.findViewById(R.id.pedidoFornEmp);
            pedidoSituacao = itemView.findViewById(R.id.pedidoSituacaoEmp);

        }
    }
}
