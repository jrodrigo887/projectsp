package com.supermarket.projeto.classes.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.FragmentsProduto.VisaoGeralProdutosFragment;

import java.util.List;

public class PagerAdapterProduto extends FragmentPagerAdapter {
    private Context context;
    private List<Fragment> fragments;
    private String[] titles;

    public PagerAdapterProduto(FragmentManager fm, List<Fragment> fragmentList, String[] mtbTitle) {
        super(fm);
        this.fragments = fragmentList;
        this.titles = mtbTitle;
    }

    @Override
    public Fragment getItem(int position) {

        return  fragments.get(position);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
