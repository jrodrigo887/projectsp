package com.supermarket.projeto.classes.Model;

import android.os.Parcelable;
import android.provider.ContactsContract;

import org.parceler.Parcel;

import java.util.Date;


public class Produto implements Parcelable {
     String titulo;
     String descricao;
     String categoria;
     String marca;
     String valor;
     String nomeMei;
     String fornecedor;
     String keyProduto;
     Date data;
     float avaliacao;
     String modelo;

    public String getKeyProduto() {
        return keyProduto;
    }

    public void setKeyProduto(String keyProduto) {
        this.keyProduto = keyProduto;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


    public String getNomeMei() {
        return nomeMei;
    }

    public void setNomeMei(String nomeMei) {
        this.nomeMei = nomeMei;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }


    public float getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(float avaliacao) {
        this.avaliacao = avaliacao;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeString(this.titulo);
        dest.writeString(this.descricao);
        dest.writeString(this.categoria);
        dest.writeString(this.marca);
        dest.writeString(this.valor);
        dest.writeString(this.nomeMei);
        dest.writeString(this.fornecedor);
        dest.writeString(this.keyProduto);
        dest.writeLong(this.data != null ? this.data.getTime() : -1);
        dest.writeFloat(this.avaliacao);
        dest.writeString(this.modelo);
    }

    public Produto() {
    }

    protected Produto(android.os.Parcel in) {
        this.titulo = in.readString();
        this.descricao = in.readString();
        this.categoria = in.readString();
        this.marca = in.readString();
        this.valor = in.readString();
        this.nomeMei = in.readString();
        this.fornecedor = in.readString();
        this.keyProduto = in.readString();
        long tmpData = in.readLong();
        this.data = tmpData == -1 ? null : new Date(tmpData);
        this.avaliacao = in.readFloat();
        this.modelo = in.readString();
    }

    public static final Parcelable.Creator<Produto> CREATOR = new Parcelable.Creator<Produto>() {
        @Override
        public Produto createFromParcel(android.os.Parcel source) {
            return new Produto(source);
        }

        @Override
        public Produto[] newArray(int size) {
            return new Produto[size];
        }
    };
}
