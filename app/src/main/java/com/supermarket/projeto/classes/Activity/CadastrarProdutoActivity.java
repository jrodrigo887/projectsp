package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Helper.Preferencias;
import com.supermarket.projeto.classes.Model.Produto;
import com.supermarket.projeto.classes.Model.Usuario;

public class CadastrarProdutoActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private BootstrapEditText tituloProduto, marcaProduto, categoriaProduto, descricaoProduto, valorProduto;
    private BootstrapButton btnCadastrar, btnCancelar;
    private FirebaseAuth autenticacao;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Usuario usuario;
    private Produto produto;
    Preferencias pref;
    private TextInputLayout iptTitulo, iptMarca, iptValor, iptDescricao, iptCategoria;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_produto);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Cadastrar Produtos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tituloProduto     = (BootstrapEditText) findViewById(R.id.pdtTitulo);
        descricaoProduto  = (BootstrapEditText) findViewById(R.id.pdtDescricao);
        categoriaProduto  = (BootstrapEditText) findViewById(R.id.pdtCategoria);
        marcaProduto      = (BootstrapEditText) findViewById(R.id.pdtMarca);
        valorProduto      = (BootstrapEditText) findViewById(R.id.pdtValor);
        btnCadastrar = (BootstrapButton) findViewById(R.id.btnCadProduto);
        btnCancelar = (BootstrapButton) findViewById(R.id.btnCancelarProd);

        iptTitulo = findViewById(R.id.iptLaoytTituloCadastro);
        iptDescricao = findViewById(R.id.iptLaoytDescricaoCadastro);
        iptCategoria = findViewById(R.id.iptLaoytCategoriaCadastro);
        iptMarca = findViewById(R.id.iptLaoytMarcaCadastro);
        iptValor = findViewById(R.id.iptLaoytValorCadastro);

        //Recuperando Usuário ativo
        usuario = new Usuario();
        pref = new Preferencias(CadastrarProdutoActivity.this);
        usuario.setEmail(pref.getEmailUsuarioLogado());
        usuario.setSenha(pref.getSenhaUsuarioLogado());

         //Button
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(tituloProduto.getText().toString().isEmpty() && descricaoProduto.getText().toString().isEmpty()
                        && valorProduto.getText().toString().isEmpty()){
                    iptTitulo.setEnabled(true);
                    iptDescricao.setEnabled(true);
                    iptValor.setEnabled(true);
                    iptTitulo.setError("* Inserir o Titulo do produto!");
                    iptDescricao.setError("* Inserir Descrição!");
                    iptValor.setError("* Inserir Valor!");

                }else{
                    iptTitulo.setErrorEnabled(false);
                    iptValor.setErrorEnabled(false);
                    iptDescricao.setErrorEnabled(false);



                    if (pref.getEmailUsuarioLogado() != null && pref.getSenhaUsuarioLogado() != null){
                        produto = new Produto();

                        produto.setTitulo(tituloProduto.getText().toString());
                        produto.setDescricao(descricaoProduto.getText().toString());
                        produto.setMarca(marcaProduto.getText().toString());
                        produto.setCategoria(categoriaProduto.getText().toString());
                        produto.setValor(valorProduto.getText().toString());
                        produto.setFornecedor(usuario.getEmail());


                        cadastrarProduto();
                    }else{
                        Toast.makeText(CadastrarProdutoActivity.this, "Senha não se Correspondem", Toast.LENGTH_LONG).show();
                    }

                }

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpaCadastro();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            //retorna para a tela anterior.
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cadastrarProduto(){

        autenticacao = ConfigFirebase.getAutenticacao();
        autenticacao.signInWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(CadastrarProdutoActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    inserirProduto(produto);

                }else{

                    String errException = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthRecentLoginRequiredException e){
                        errException = "Tentantiva de se logar novamente";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        errException = "Dados digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e){
                        errException = "Dados já está cadastrado!";
                    } catch (Exception e) {
                        errException = "Erro ao efetuar o cadastro";
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastrarProdutoActivity.this, "Erro! " + errException, Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    FirebaseAuth.AuthStateListener minhaAuthListener;

    private boolean inserirProduto(Produto pdt){

        try{
            minhaAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                }
            };
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String uid = user.getUid();

            //acessa a chave a base do BD para adicionar os produtos

            databaseReference = ConfigFirebase.getReferenceFirebase().child("produtos");

            //inserindo a chave única do produto, criada pelo firebase.
            String chaveKey = databaseReference.push().getKey();
            pdt.setKeyProduto(chaveKey);

            //Inserindo dados na chave antes armazenada na variável key.
            databaseReference.push().setValue(pdt);


            Toast.makeText(CadastrarProdutoActivity.this, "Produto cadastrado com Sucesso!" , Toast.LENGTH_LONG).show();

            abriTeladeRetorno();
            return true;
        }catch (Exception e){
            Toast.makeText(CadastrarProdutoActivity.this, "Erro ao gravar o produto" , Toast.LENGTH_LONG).show();

            return false;
        }
    }

    private void abriTeladeRetorno(){

        tituloProduto.setText("");
        descricaoProduto.setText("");
        categoriaProduto.setText("");
        marcaProduto.setText("");
        valorProduto.setText("");


    }

    // Método que usarei posteriomente
    private void verificarUsuario(){

        //Puxar os dados do banco para verificar o tipo de usuário
        databaseReference.child("usuarios").child("usuarios").orderByChild("email").equalTo(usuario.getEmail()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    //No usuario terá um array de produtos
                    if (postSnapshot.child("tipoUsuario").getValue()!=null){
                        //quero buscar adicionar os produtos relacionado ao usuário ativo.
                        String tipoUsuarioEmail = postSnapshot.child("tipoUsuario").getValue().toString();
                        Log.d("principal", "Tipo usuario" + tipoUsuarioEmail);



                    }else{
                        Toast.makeText(CadastrarProdutoActivity.this, "Tipo de usuário não identificado", Toast.LENGTH_LONG).show();
//                        tipoUsuario.setText("Usuário Padrão");
//                        getMenuInflater().inflate(R.menu.menu_padrao,menu1);

                        Intent intent = new Intent(CadastrarProdutoActivity.this, PrincipalActivity.class);
                        finish();
                        startActivity(intent);

                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Intent intent = new Intent(CadastrarProdutoActivity.this, PrincipalActivity.class);
        finish();
        startActivity(intent);

    }

    public void limpaCadastro(){

        tituloProduto.setText("");
        descricaoProduto.setText("");
        categoriaProduto.setText("");
        marcaProduto.setText("");
        valorProduto.setText("");

    }

}
