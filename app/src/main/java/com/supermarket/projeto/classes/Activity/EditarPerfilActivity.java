package com.supermarket.projeto.classes.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.supermarket.projeto.R;
import com.supermarket.projeto.classes.DAO.ConfigFirebase;
import com.supermarket.projeto.classes.Model.Endereco;
import com.supermarket.projeto.classes.Model.Usuario;

public class EditarPerfilActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private BootstrapEditText editNome;

    private BootstrapEditText editCpf;
    private BootstrapEditText editTelefone;
    private BootstrapEditText editCelular;
    private BootstrapEditText editTipo;
    private BootstrapEditText editSenha1, editSenha2;
    private BootstrapEditText editSexo;
    private BootstrapEditText editRua, editNumero, editComplemento, editCep, editBairro, editCidade, editEstado;

    private RadioButton rbMasculino, rbFeminino;
    private BootstrapButton btnSalvar;
    private BootstrapButton btnCancelar;
    private Spinner tipoUserSpinner;
    private String userString;

    //metodos strings para enviar por meio do Bundle.
    private String strNome="",strEmail="", strTipoUser="", strCpf="",
            strCelular="", strRua="", strNum="", strComplemento="", strTelefone="",
            strCep="", strBairro="", strCidade="", strEstado="", dadosEditar="", strkeyUsuario="";

    //--------------------------------------------------------------

    private DatabaseReference reference;
    private FirebaseAuth autenticacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editNome = (BootstrapEditText)  findViewById(R.id.newNomePerfil);
        editCpf = (BootstrapEditText)  findViewById(R.id.newCpfPerfil);
        editTelefone = (BootstrapEditText)  findViewById(R.id.newTelefonePerfil);
        editCelular = (BootstrapEditText)  findViewById(R.id.newCelularPerfil);
        tipoUserSpinner = (Spinner)  findViewById(R.id.spinnerTipoUser);
        editSenha1 = (BootstrapEditText) findViewById(R.id.newSenhaPerfil);
        editSenha2 = (BootstrapEditText) findViewById(R.id.newSenha2Perfil);

        //Pegando ids Endereço
        editRua = (BootstrapEditText) findViewById(R.id.newRuaPerfil);
        editNumero = (BootstrapEditText) findViewById(R.id.newNumeroPerfil);
        editComplemento = (BootstrapEditText) findViewById(R.id.newComplementoPerfil);
        editCep = (BootstrapEditText) findViewById(R.id.newCepPerfil);
        editBairro = (BootstrapEditText) findViewById(R.id.newBairroPerfil);
        editCidade = (BootstrapEditText) findViewById(R.id.newCidadePerfil);
        editEstado = (BootstrapEditText) findViewById(R.id.newEstadoPerfil);
        btnSalvar = (BootstrapButton) findViewById(R.id.newSalvarPerfil);
        btnCancelar = (BootstrapButton) findViewById(R.id.newCancelarPerfil);



        //Spinner para tipo de usuários.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(EditarPerfilActivity.this,
                R.array.array_tipo_usuario, R.layout.support_simple_spinner_dropdown_item );
        tipoUserSpinner.setAdapter(adapter);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        dadosEditar = bundle.getString("inicio");

        if (dadosEditar.equals("dadosEditar")){

            //Strings recebendo valores do Bundle
            strNome = bundle.getString("nome");
            strEmail = bundle.getString("email");
            strCpf = bundle.getString("cpf");
            strTipoUser =   bundle.getString("tipoUsuario");
            strkeyUsuario = bundle.getString("chave");


            strCelular =   bundle.getString("celular");
            strTelefone =   bundle.getString("telefone");
            strRua =   bundle.getString("rua");
            strNum =   bundle.getString("numero");
            strComplemento =   bundle.getString("complemento");
            strCep =   bundle.getString("cep");
            strBairro =   bundle.getString("bairro");
            strCidade =   bundle.getString("cidade");
            strEstado =   bundle.getString("estado");

            editNome.setText(strNome);
            editCpf.setText(strCpf);
            editTelefone.setText(strTelefone);
            editCelular.setText(strCelular);

            //Endereço
            editRua.setText(strRua);
            editComplemento.setText(strComplemento);
            editCep.setText(strCep);
            editBairro.setText(strBairro);
            editCidade.setText(strCidade);
            editEstado.setText(strEstado);
            editNumero.setText(strNum);
        }


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tipoUsuarioSpinner();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaPrincipal();

            }
        });



    }


    private void tipoUsuarioSpinner(){


        userString = tipoUserSpinner.getSelectedItem().toString();
        if (userString.equals("Tipo de Usuário")){
            Toast.makeText(EditarPerfilActivity.this,"Selecione o tipo de usuário! " , Toast.LENGTH_LONG).show();
            tipoUserSpinner.setFocusable(true);
            tipoUserSpinner.setBackgroundColor(getTitleColor());


        }else{


                //Verificando a Senha se é igual
            if (editSenha1.getText().toString()
                    .equals(editSenha2.getText().toString()) && editSenha1.getText().toString() != "" && editSenha2.getText().toString() != ""){
                Toast.makeText(EditarPerfilActivity.this,"Tipo Usuário: " + userString, Toast.LENGTH_LONG).show();

                Usuario usuario = new Usuario();
                usuario.setNome(editNome.getText().toString());
                usuario.setCnpj(editCpf.getText().toString());
                usuario.setTelefone(editTelefone.getText().toString());
                usuario.setCelular(editCelular.getText().toString());
                usuario.setKeyUsuario(strkeyUsuario);
                usuario.setEmail(strEmail);

//                if (rbMasculino.isChecked()){
//                    usuario.setSexo("Masculino");
//
//                }else if (rbFeminino.isChecked()){
//                    usuario.setSexo("Feminino");
//                }
                usuario.setSenha(editSenha1.getText().toString());

                strTipoUser = tipoUserSpinner.getSelectedItem().toString();
                if (strTipoUser.equals("Selecionar")){
                    Toast.makeText(EditarPerfilActivity.this,"Selecione o Tpo de Usuário! " , Toast.LENGTH_LONG).show();


                }else{

                    usuario.setTipoUsuario(strTipoUser);


                }


                //Endereço
                Endereco endereco = new Endereco();
                endereco.setRua(editRua.getText().toString());
                endereco.setComplemeto(editComplemento.getText().toString());
                endereco.setCep(editCep.getText().toString());
                endereco.setBairro(editBairro.getText().toString());
                endereco.setCidade(editCidade.getText().toString());
                endereco.setUf(editEstado.getText().toString());
                endereco.setNumero(editNumero.getText().toString());
                usuario.setEndereco(endereco);

                AtualizarPerfil(usuario); //Senha correta? então, cadastrar usuário!

            }else{
                Toast.makeText(EditarPerfilActivity.this, "Senhas incorretas ou vazia por favor verifique!", Toast.LENGTH_LONG).show();
                btnSalvar.setEnabled(true);
            }
        }







    }

    private boolean AtualizarPerfil(final Usuario usuario) {

        btnSalvar.setEnabled(false);

        try{

            reference = ConfigFirebase.getReferenceFirebase();

            atualizarSenha(usuario.getSenha());

            reference.child("usuarios").child(usuario.getKeyUsuario()).setValue(usuario);

            Toast.makeText(EditarPerfilActivity.this, "Dados alterados com Sucesso!", Toast.LENGTH_LONG).show();

            abrirTelaPrincipal();





        }catch(Exception e){
            e.printStackTrace();
        }


        return true;

    }

    private void abrirTelaPrincipal(){
        Intent intent = new Intent(EditarPerfilActivity.this, PerfilUsuarioActivity.class);
        startActivity(intent);

        finish();

    }

    //Atualizar com a nova senha.
    private void atualizarSenha(String senhaNova){

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        user.updatePassword(senhaNova).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                    Log.d("nova_senha","Senha atualizada com sucesso!");
                }


            }
        });


    }


}
